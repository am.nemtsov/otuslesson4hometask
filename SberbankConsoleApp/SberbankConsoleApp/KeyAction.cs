﻿using SberbankPostgresContext;
using System;
using System.Collections.Generic;

namespace SberbankApp
{
    internal static class KeyAction
    {
        internal static Dictionary<int, Action> Relation = new Dictionary<int, Action>
        {
            { 1, ShowAccounts},
            { 2, ShowCustomers},
            { 3, ShowFinancialTransactions},
            { 4, CreateAccount},
            { 5, CreateCustomer},
            { 6, CreateFinancialTransaction},
        };

        internal async static void ShowAccounts()
        {
            var accountService = new AccountService();
            var accounts = await accountService.GetAllAsync();
            Console.WriteLine($"\n\n Account \n");
            foreach (var account in accounts)
            {
                Console.WriteLine($"{nameof(account.AccountId)} : {account.AccountId},  {nameof(account.Login)} : {account.Login}, {nameof(account.Password)} : {account.Password} ");
            }
        }

        internal async static void ShowCustomers()
        {
            var customerService = new CustomerService();
            var customers = await customerService.GetAllAsync();
            Console.WriteLine($"\n\n Customer: \n");
            foreach (var customer in customers)
            {
                Console.WriteLine($"{nameof(customer.CustomerId)} : {customer.CustomerId}, {nameof(customer.AccountId)} : {customer.AccountId}, {nameof(customer.Email)} : {customer.Email}");
            }
        }

        internal async static void ShowFinancialTransactions()
        {
            var financialTransactionService = new FinancialTransactionService();
            var financialTransactions = await financialTransactionService.GetAllAsync();
            Console.WriteLine($"\n\n FinancialTransaction: \n");
            foreach (var financialTransaction in financialTransactions)
            {
                Console.WriteLine($"{nameof(financialTransaction.FinancialTransactionId)} : {financialTransaction.FinancialTransactionId}, {nameof(financialTransaction.Description)} : {financialTransaction.Description},   {nameof(financialTransaction.CustomerId)} : {financialTransaction.CustomerId}  {nameof(financialTransaction.TimeOperation)} : {financialTransaction.TimeOperation} ");
            }
        }

        internal async static void CreateAccount()
        {
            Console.WriteLine("\nВведите логин:");
            var login = Console.ReadLine();

            Console.WriteLine("Введите пароль:");
            var password = Console.ReadLine();

            var account = new Account
            {
                Login = login,
                Password = password,
            };
            var accountService = new AccountService();
            var idAccount = await accountService.CreateAsync(account);

            Console.WriteLine($"Account был успешно создан в БД с ID {idAccount}:");
        }

        internal async static void CreateCustomer()
        {
            var customerService = new CustomerService();
            var accountService = new AccountService();

                Console.WriteLine("\nВведите имя:");
            var userName = Console.ReadLine();
            Console.WriteLine("Введите Email:");
            var email = Console.ReadLine();

            var customer = new Customer
            {
                Username = userName,
                Email = email,
            };

            Console.WriteLine("У пользователя есть ссылка на аккаунт: 1 - да");
            var consoleAnswer = Console.ReadKey();
            var isUserAnswerInteger = int.TryParse(consoleAnswer.KeyChar.ToString(), out int result);
            if (isUserAnswerInteger && result == 1)
            {
                Console.WriteLine("\n Введите корректный идентификатор аккаунта!");
                consoleAnswer = Console.ReadKey();
                isUserAnswerInteger = int.TryParse(consoleAnswer.KeyChar.ToString(), out var IdAccountResult);
                while(!await accountService.СontainAccountAsync(IdAccountResult))
                {
                    Console.WriteLine($"\n Ведденый идентификатор аккаунта отсуствует в БД!");
                    Console.WriteLine("Введите корректный идентификатор аккаунта!");
                    consoleAnswer = Console.ReadKey();
                    isUserAnswerInteger = int.TryParse(consoleAnswer.KeyChar.ToString(), out IdAccountResult);
                }
                customer.AccountId = IdAccountResult;
            }

            var idCustomer = await customerService.CreateAsync(customer);
            Console.WriteLine($"\n Customer был успешно создан в БД с ID {idCustomer}:");
        }

        internal async static void CreateFinancialTransaction()
        {
            Console.WriteLine("\nВведите описание транзакции:");
            var descrition = Console.ReadLine();

            var financialTransaction = new FinancialTransaction
            {
                Description = descrition,
            };
            var financialTransactionService = new FinancialTransactionService();
            var idFinancialTransactionService = await financialTransactionService.CreateAsync(financialTransaction);

            Console.WriteLine($"FinancialTransaction был успешно создан в БД с ID {idFinancialTransactionService}:");
        }
    }
}
