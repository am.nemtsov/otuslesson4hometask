﻿using Microsoft.EntityFrameworkCore;
using SberbankPostgresContext;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SberbankApp
{
    internal class AccountService
    {
        private readonly PostgresContext _postgresContext;
        internal AccountService()
        {
            _postgresContext = new PostgresContext();
        }

        internal async Task<IList<Account>> GetAllAsync()
        {
           return await _postgresContext.Accounts.ToListAsync();
        }

        internal async Task<int> CreateAsync(Account account)
        {
            await _postgresContext.AddAsync(account);
            await _postgresContext.SaveChangesAsync();
            return account.AccountId;
        }

        internal async Task<bool> СontainAccountAsync(int idAccount)
        {
            return await _postgresContext.Accounts.AnyAsync(a => a.AccountId == idAccount);
        }
    }
}
