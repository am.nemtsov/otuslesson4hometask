﻿using SberbankApp;
using SberbankPostgresContext;
using System;
using System.Threading.Tasks;

namespace SberbankConsoleApp
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello in sberbank app!");
            using var postgres = new PostgresContext();
            await postgres.CreateTablesAsync();
            await postgres.FillDefaultDataAsync();
            StartUserDialog();
        }

        private static void StartUserDialog()
        {
            while (true)
            {
                Console.WriteLine("\nВыберете комманду из списка: \n 1 - Прочитать данные из таблицы Account \n 2 - Прочитать данные из таблицы Customer \n 3 - Прочитать данные из таблицы FinancialTransaction" +
                    " \n 4 - Создать запись в таблице Account\n 5 - Создать запись в таблице Customer \n 6 - Создать запись в таблице FinancialTransaction");
                var consoleAnswer = Console.ReadKey();
                var isUserAnswerInteger = int.TryParse(consoleAnswer.KeyChar.ToString(), out int result);
                if (isUserAnswerInteger && KeyAction.Relation.ContainsKey(result))
                {
                    KeyAction.Relation[result]();
                }
                else
                {
                    Console.WriteLine("\n\n Введите корректную комманду! \n");
                }
            }
        }
    }
}
