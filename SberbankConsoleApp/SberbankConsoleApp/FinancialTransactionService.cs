﻿using Microsoft.EntityFrameworkCore;
using SberbankPostgresContext;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SberbankApp
{
    internal class FinancialTransactionService
    {
        private readonly PostgresContext _postgresContext;
        internal FinancialTransactionService()
        {
            _postgresContext = new PostgresContext();
        }

        internal async Task<IList<FinancialTransaction>> GetAllAsync()
        {
           return await _postgresContext.FinancialTransactions.ToListAsync();
        }

        internal async Task<int> CreateAsync(FinancialTransaction FinancialTransaction)
        {
            await  _postgresContext.AddAsync(FinancialTransaction);
            await _postgresContext.SaveChangesAsync();
            return FinancialTransaction.FinancialTransactionId;
        }

        internal async Task<bool> СontainAccountAsync(int idFinancialTransaction)
        {
            return await _postgresContext.FinancialTransactions.AnyAsync(a => a.FinancialTransactionId == idFinancialTransaction);
        }
    }
}
