﻿using Microsoft.EntityFrameworkCore;
using SberbankPostgresContext;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SberbankApp
{
    internal class CustomerService
    {
        private readonly PostgresContext _postgresContext;
        internal CustomerService()
        {
            _postgresContext = new PostgresContext();
        }

        internal async Task<IList<Customer>> GetAllAsync()
        {
           return await _postgresContext.Customers.ToListAsync();
        }

        internal async Task<int> CreateAsync(Customer Customer)
        {
            await _postgresContext.AddAsync(Customer);
            await _postgresContext.SaveChangesAsync();
            return Customer.CustomerId;
        }

        internal async Task<bool> СontainCustomerAsync(int idCustomer)
        {
            return await _postgresContext.Customers.AnyAsync(a => a.CustomerId ==idCustomer);
        }
    }
}
