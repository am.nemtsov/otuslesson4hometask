﻿using System.Collections.Generic;

namespace SberbankPostgresContext
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public int? AccountId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        public Account Account { get; set; }
        public List<FinancialTransaction> FinancialTransactions { get; set; }
    }
}
