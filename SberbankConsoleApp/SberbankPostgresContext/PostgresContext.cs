﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SberbankPostgresContext
{
    public class PostgresContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<FinancialTransaction> FinancialTransactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=sberbank;Username=postgres;Password=postgres");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FinancialTransaction>()
                .Property(b => b.TimeOperation)
                .HasDefaultValueSql("now()");
        }

        public async Task CreateTablesAsync() 
        {
            await Database.EnsureDeletedAsync();
            await Database.EnsureCreatedAsync();
        }

        public async Task FillDefaultDataAsync()
        {
            var accounts = new List<Account>
            {
                new Account{ Login = "SberbankLogin№1", Password = "somePassword" },
                new Account{ Login = "SberbankLogin№2", Password = "somePassword" },
                new Account{ Login = "SberbankLogin№3", Password = "somePassword" },
            };
            await Accounts.AddRangeAsync(accounts);
            await SaveChangesAsync();

            var customers = new List<Customer>
            {
                new Customer{ Account = accounts[0], Username = "SberbankUserName№1", Email = "someEmail"},
                new Customer{ Username = "SberbankUserName№2"},
                new Customer{ Account = accounts[1], Username = "SberbankUserName№3", Email = "someEmail"},
            };
            await Customers.AddRangeAsync(customers);
            await SaveChangesAsync();

            var financialTransaction = new List<FinancialTransaction>
            {
                new FinancialTransaction {Customer = customers[0], Description = "someDescriptionTransaction1"},
                new FinancialTransaction {Customer = customers[1], Description = "someDescriptionTransaction2"},
                new FinancialTransaction {Customer = customers[2], Description = "someDescriptionTransaction3"},
            };
            await FinancialTransactions.AddRangeAsync(financialTransaction);
            await SaveChangesAsync();
        }
    }
}
