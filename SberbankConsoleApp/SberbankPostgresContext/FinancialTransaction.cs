﻿using System;

namespace SberbankPostgresContext
{
    public class FinancialTransaction
    {
        public int FinancialTransactionId { get; set; }
        public int? CustomerId { get; set; }
        public string Description { get; set; }
        public DateTime TimeOperation { get; set; }

        public Customer Customer { get; set; }
    }
}
