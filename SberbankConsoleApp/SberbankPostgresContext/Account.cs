﻿using System.Collections.Generic;

namespace SberbankPostgresContext
{
    public class Account
    {
        public int AccountId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public List<Customer> Customers { get; set; }
    }
}
