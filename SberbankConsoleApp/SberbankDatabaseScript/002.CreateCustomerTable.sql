DROP TABLE IF EXISTS Customers;

CREATE TABLE Customers (
	customer_id serial PRIMARY KEY,
	account_id INT,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	email VARCHAR ( 255 ),
	CONSTRAINT fk_account
      FOREIGN KEY(account_id) 
	  REFERENCES Accounts(account_id)
);