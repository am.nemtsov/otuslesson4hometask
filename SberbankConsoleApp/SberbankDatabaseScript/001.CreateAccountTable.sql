DROP TABLE IF EXISTS Accounts;

CREATE TABLE Accounts (
	account_id serial PRIMARY KEY,
	login VARCHAR ( 255 ) UNIQUE NOT NULL,
	password VARCHAR ( 255 ) NOT NULL
);