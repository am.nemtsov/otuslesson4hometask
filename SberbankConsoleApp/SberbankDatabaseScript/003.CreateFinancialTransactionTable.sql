DROP TABLE IF EXISTS FinancialTransactions;

CREATE TABLE FinancialTransactions (
	financial_transaction_id serial PRIMARY KEY,
	customer_id INT NOT NULL,
	description VARCHAR (255),
	time_operation TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT fk_customer
      FOREIGN KEY(customer_id) 
	  REFERENCES Customer(customer_id)
);